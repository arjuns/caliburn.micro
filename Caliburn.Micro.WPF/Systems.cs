﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Caliburn.Micro
{
    public delegate TResult Func<in T1, in T2, in T3, in T4, in T5, out TResult>(
        T1 arg1, T2 arg2, T3 arg3, T4 arg4, T5 arg5);

    public delegate void Action<in T1, in T2, in T3, in T4, in T5, in T6>(
        T1 arg1, T2 arg2, T3 arg3, T4 arg4, T5 arg5, T6 arg6);

    public static class EnumerableExtension
    {

        public static IEnumerable<TResult> Zip<T1, T2, TResult>(this IEnumerable<T1> source1, IEnumerable<T2> source2,

                                                                Func<T1, T2, TResult> func)
        {

            using (var iter1 = source1.GetEnumerator())

            using (var iter2 = source2.GetEnumerator())

            {
                while (iter1.MoveNext() && iter2.MoveNext())
                {
                    yield return func(iter1.Current, iter2.Current);
                }
            }

        }

    }
}

